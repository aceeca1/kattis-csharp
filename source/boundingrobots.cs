using System;

class boundingrobots {
    public static void Main() {
        while (true) {
            var line = Console.ReadLine().Split();
            var w = int.Parse(line[0]);
            if (w == 0) break;
            var l = int.Parse(line[1]);
            int factX = 0, factY = 0, idealX = 0, idealY = 0;
            var n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; ++i) {
                line = Console.ReadLine().Split();
                var length = int.Parse(line[1]);
                switch (line[0][0]) {
                    case 'r': 
                        idealX += length; 
                        factX = Math.Min(factX + length, w - 1); break;
                    case 'u':
                        idealY += length;
                        factY = Math.Min(factY + length, l - 1); break;
                    case 'l':
                        idealX -= length;
                        factX = Math.Max(factX - length, 0); break;
                    case 'd':
                        idealY -= length;
                        factY = Math.Max(factY - length, 0); break;
                }
            }
            Console.WriteLine("Robot thinks {0} {1}", idealX, idealY);
            Console.WriteLine("Actually at {0} {1}", factX, factY);
            Console.WriteLine();
        }
    }
}
