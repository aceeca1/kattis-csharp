using System;

class synchronizinglists {
    public static void Main() {
        bool head = true;
        while (true) {
            var n = int.Parse(Console.ReadLine());
            if (n == 0) return;
            if (!head) Console.WriteLine();
            head = false;
            var a1 = new int[n];
            for (int i = 0; i < n; ++i) a1[i] = int.Parse(Console.ReadLine());
            var a2 = new int[n];
            for (int i = 0; i < n; ++i) a2[i] = int.Parse(Console.ReadLine());
            var b = new int[n];
            for (int i = 0; i < n; ++i) b[i] = i;
            Array.Sort(a1, b);
            Array.Sort(a2);
            for (int i = 0; i < n; ++i) a1[b[i]] = a2[i];
            for (int i = 0; i < n; ++i) Console.WriteLine(a1[i]);
        }
    }
}
