using System;

class mixedfractions {
    public static void Main() {
        while (true) {
            var line = Console.ReadLine().Split();
            var n = int.Parse(line[0]);
            var k = int.Parse(line[1]);
            if (n == 0) return;
            Console.WriteLine("{0} {1} / {2}", n / k, n % k, k);
        }
    }
}
