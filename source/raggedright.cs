using System;
using System.Collections.Generic;
using System.Linq;

class raggedright {
    public static void Main() {
        var a = new List<int>();
        while (true) {
            var line = Console.ReadLine();
            if (line == null) break;
            a.Add(line.Length);
        }
        var max = a.Max();
        var answer = 0;
        for (int i = 0; i < a.Count - 1; ++i)
            answer += (max - a[i]) * (max - a[i]);
        Console.WriteLine(answer);
    }
}
