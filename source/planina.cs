using System;

class planina {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = (1 << n) + 1;
        Console.WriteLine(a * a);
    }
}
