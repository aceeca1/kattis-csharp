using System;
using System.Linq;

class zamka {
    static int SumOfDigits(int n) => n.ToString().Sum(k => k - '0');
    
    public static void Main() {
        var l = int.Parse(Console.ReadLine());
        var d = int.Parse(Console.ReadLine());
        var x = int.Parse(Console.ReadLine());
        int n = l, m = d;
        while (SumOfDigits(n) != x) ++n;
        while (SumOfDigits(m) != x) --m;
        Console.WriteLine(n);
        Console.WriteLine(m);
    }
}
