using System;
using System.Numerics;

class insert {
    class BSTNode {
        public BSTNode ChildL = null, ChildR = null;
        public int Value;
    }
    
    static void Insert(BSTNode root, int k) {
        while (true) {
            if (k < root.Value) {
                if (root.ChildL == null) {
                    root.ChildL = new BSTNode { Value = k };
                    break;
                } else root = root.ChildL;
            } else {
                if (root.ChildR == null) {
                    root.ChildR = new BSTNode { Value = k };
                    break;
                } else root = root.ChildR;
            }
        }
    }

    static BigInteger Choose(int a1, int a2) {
        var answer = BigInteger.One;
        for (int i = 1; i <= a2; ++i) answer = answer * (a1 + 1 - i) / i;
        return answer;
    }
    
    static BigInteger Ways(BSTNode root, out int size) {
        size = 0;
        if (root == null) return 1;
        int sizeL, sizeR;
        var answerL = Ways(root.ChildL, out sizeL);
        var answerR = Ways(root.ChildR, out sizeR);
        size = sizeL + sizeR + 1;
        return answerL * answerR * Choose(sizeL + sizeR, sizeR);
    }
    
    public static void Main() {
        while (true) {
            var n = int.Parse(Console.ReadLine());
            if (n == 0) break;
            var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
            var root = new BSTNode { Value = a[0] };
            for (int i = 1; i < a.Length; ++i) Insert(root, a[i]);
            int z;
            Console.WriteLine(Ways(root, out z));
        }
    }
}
