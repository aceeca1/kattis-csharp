using System;

class fizzbuzz {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var x = int.Parse(line[0]);
        var y = int.Parse(line[1]);
        var n = int.Parse(line[2]);
        for (int i = 1; i <= n; ++i) {
            bool fizz = i % x == 0;
            bool buzz = i % y == 0;
            if (fizz) Console.WriteLine(buzz ? "FizzBuzz" : "Fizz");
            else      Console.WriteLine(buzz ? "Buzz" : i.ToString());
        }
    }
}
