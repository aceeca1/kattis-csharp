using System;
using System.Collections.Generic;
using System.Numerics;

class tsp {
    static void VisitTree(IGraph<ICostEdge<double>> graph, int no) {
        Console.WriteLine(no);
        foreach (var i in graph.OutEdge(no)) VisitTree(graph, i.Target());
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new Complex[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var x = double.Parse(line[0]);
            var y = double.Parse(line[1]);
            a[i] = new Complex(x, y);
        }
        var graph = new EdgeListGraph<ICostEdge<double>>();
        for (int i = 0; i < n; ++i) 
            graph.Out.Add(new List<ICostEdge<double>>());
        for (int i = 0; i < n; ++i)
            for (int j = i + 1; j < n; ++j) {
                var cost = Complex.Abs(a[i] - a[j]);
                graph.Out[i].Add(new CostEdge<double> {T = j, C = cost});
                graph.Out[j].Add(new CostEdge<double> {T = i, C = cost});
            }
        var mst = new MinSpanTree<double> {
            Graph = graph,
            Ops = new OpsDouble()
        };
        mst.Solve(new MSTQueueDense<double>());
        VisitTree(mst.Tree, mst.TreeRoot);
    }
}
