using System;

class ladder {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var h = int.Parse(line[0]);
        var v = int.Parse(line[1]);
        var answer = Math.Ceiling(h / Math.Sin(v * (Math.PI / 180.0)));
        Console.WriteLine("{0:F0}", answer);
    }
}
