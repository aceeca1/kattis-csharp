using System;
using System.Linq;

class pet {
    public static void Main() {
        var a = new int[5];
        for (int i = 0; i < a.Length; ++i)
            a[i] = Console.ReadLine().Split().Sum(int.Parse);
        var k = Array.IndexOf(a, a.Max());
        Console.WriteLine("{0} {1}", k + 1, a[k]);
    }
}
