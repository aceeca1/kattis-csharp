using System;
using System.Collections.Generic;

class symmetricorder {
    public static void Main() {
        for (int caseNo = 1;; ++caseNo) {
            var n = int.Parse(Console.ReadLine());
            if (n == 0) break;
            Console.WriteLine("SET {0}", caseNo);
            var a1 = new List<string>();
            var a2 = new List<string>();
            for (int i = 0; i < n; ++i) 
                if ((i & 1) == 0) a1.Add(Console.ReadLine());
                else              a2.Add(Console.ReadLine());
            for (int i = 0; i < a1.Count;      ++i) Console.WriteLine(a1[i]);
            for (int i = a2.Count - 1; i >= 0; --i) Console.WriteLine(a2[i]);
        }
    }
}
