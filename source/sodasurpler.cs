using System;

class sodasurpler {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var e = int.Parse(line[0]);
        var f = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        var emptyBottles = e + f;
        int drunk = 0;
        while (emptyBottles >= c) {
            var bottles = emptyBottles / c;
            drunk += bottles;
            emptyBottles = emptyBottles % c + bottles;
        }
        Console.WriteLine(drunk);
    }
}
