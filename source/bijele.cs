using System;

class bijele {
    static int[] CorrectBijele = new int[] {1, 1, 2, 2, 2, 8};
    
    public static void Main() {
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        for (int i = 0; i < a.Length; ++i) a[i] = CorrectBijele[i] - a[i];
        Console.WriteLine(string.Join(" ", a));
    }
}
