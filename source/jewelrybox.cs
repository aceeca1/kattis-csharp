using System;

class jewelrybox {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var x = double.Parse(line[0]);
            var y = double.Parse(line[1]);
            var a = 12.0;
            var b = -4.0 * (x + y);
            var c = x * y;
            var delta = b * b - 4 * a * c;
            var h = (- b - Math.Sqrt(delta)) / (a + a);
            Console.WriteLine(h * (x - h - h) * (y - h - h));
        }
    }
}
