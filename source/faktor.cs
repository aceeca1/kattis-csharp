using System;

class faktor {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var i = int.Parse(line[1]);
        Console.WriteLine(a * (i - 1) + 1);
    }
}
