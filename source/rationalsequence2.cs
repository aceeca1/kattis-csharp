using System;

class rationalsequence2 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var k = int.Parse(line[0]);
            var line1 = line[1].Split('/');
            var p = int.Parse(line1[0]);
            var q = int.Parse(line1[1]);
            int answer = 0, u = 1;
            while (p != 1 || q != 1) {
                if (p < q) q -= p;
                else { p -= q; answer += u; }
                u += u;
            }
            Console.WriteLine("{0} {1}", k, answer + u);
        }
    }
}
