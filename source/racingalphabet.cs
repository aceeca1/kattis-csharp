using System;

class racingalphabet {
    const string Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ '";
    static int[] LetterIndex = new int[256];
    
    public static void Main() {
        for (int i = 0; i < Letters.Length; ++i) LetterIndex[Letters[i]] = i;
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            int answer = 0, current = LetterIndex[s[0]];
            foreach (var j in s) {
                var next = LetterIndex[j];
                var distance = Math.Abs(next - current);
                current = next;
                if (14 < distance) distance = 28 - distance;
                answer += distance;
            }
            Console.WriteLine(answer * (Math.PI / 7) + s.Length);
        }
    }
}
