using System;
using System.Collections.Generic;
using System.Text;

class falsesecurity {
    const string Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_,.?";
    static string[] Morse = {
        ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", 
        "....", "..", ".---", "-.-", ".-..", "--", "-.", 
        "---", ".--.", "--.-", ".-.", "...", "-", 
        "..-", "...-", ".--", "-..-", "-.--", "--..", 
        "..--", ".-.-", "---.", "----"
    };
    static string[] ToMorse = new string[256];
    static Dictionary<string, char> FromMorse = new Dictionary<string, char>();
    
    static void Encode(string s, out StringBuilder morse, out int[] length) {
        morse = new StringBuilder();
        length = new int[s.Length];
        for (int i = 0; i < s.Length; ++i) {
            var m = ToMorse[s[i]];
            morse.Append(m);
            length[i] = m.Length;
        }
    }
    
    static char[] Decode(StringBuilder morse, int[] length) {
        int k = 0;
        var answer = new char[length.Length];
        for (int i = 0; i < answer.Length; ++i) {
            answer[i] = FromMorse[morse.ToString(k, length[i])];
            k += length[i];
        }
        return answer;
    }
    
    public static void Main() {
        for (int i = 0; i < Letters.Length; ++i) {
            ToMorse[Letters[i]] = Morse[i];
            FromMorse[Morse[i]] = Letters[i];
        }
        while (true) {
            var line = Console.ReadLine();
            if (line == null) break;
            StringBuilder morse;
            int[] length;
            Encode(line, out morse, out length);
            Array.Reverse(length);
            Console.WriteLine(Decode(morse, length));
        }
    }
}
