using System;

class apaxiaaans {
	class DeduplicateChars: Partition<char, char> {
		protected override bool Equal(char c1, char c2) => c1 == c2;
		protected override char Single(char c) => c;
		protected override char Add(char previous, char c) => c;
	}
	
    public static void Main() {
        var answer = new DeduplicateChars().Reduced(Console.ReadLine());
        Console.WriteLine(string.Concat(answer));
    }
}
