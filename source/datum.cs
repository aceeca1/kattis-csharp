using System;

class datum {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var d = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var date = new DateTime(2009, m, d);
        Console.WriteLine(date.DayOfWeek);
    }
}
