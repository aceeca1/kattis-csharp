using System;

class bela {
    const string NumberCard = "AKQJT987";
    static int[] NumberRank = new int[256];
    static int[]    DominantValue = {11, 4, 3, 20, 10, 14, 0, 0};
    static int[] NonDominantValue = {11, 4, 3, 2,  10,  0, 0, 0};
    
    public static void Main() {
        for (int i = 0; i < NumberCard.Length; ++i) 
            NumberRank[NumberCard[i]] = i;
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]) << 2;
        var b = line[1][0];
        int answer = 0;
        for (int i = 0; i < n; ++i) {
            var line1 = Console.ReadLine();
            answer += line1[1] == b ?    DominantValue[NumberRank[line1[0]]]
                                    : NonDominantValue[NumberRank[line1[0]]];
        }
        Console.WriteLine(answer);
    }
}
