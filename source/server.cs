using System;

class server {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var t = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var k = 0;
        while (k < n && a[k] <= t) t -= a[k++];
        Console.WriteLine(k);
    }
}
