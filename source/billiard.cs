using System;

class billiard {
    public static void Main() {
        while (true) {
            var line = Console.ReadLine().Split();
            var a = double.Parse(line[0]);
            var b = double.Parse(line[1]);
            var s = double.Parse(line[2]);
            var m = double.Parse(line[3]);
            var n = double.Parse(line[4]);
            if (a == 0.0) break;
            var x = a * m;
            var y = b * n;
            var z = Math.Sqrt(x * x + y * y);
            var angle = Math.Atan2(y, x) * (180.0 / Math.PI);
            Console.WriteLine("{0:F2} {1:F2}", angle, z / s);
        }
    }
}
