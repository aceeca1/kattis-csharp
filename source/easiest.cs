using System;
using System.Linq;

class easiest {
    static int SumOfDigits(int n) => n.ToString().Sum(k => k - '0');
    
    public static void Main() {
        while (true) {
            var n = int.Parse(Console.ReadLine());
            if (n == 0) break;
            var s = SumOfDigits(n);
            var p = 11;
            while (SumOfDigits(p * n) != s) ++p;
            Console.WriteLine(p);
        }
    }
}
