using System;
using System.Linq;

class heartrate {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var b = int.Parse(line[0]);
            var p = double.Parse(line[1]);
            var answer = Enumerable.Range(b - 1, 3).Select(k => 60 * k / p);
            Console.WriteLine(string.Join(" ", answer));
        }
    }
}
