using System;

class peg {
    static char[][] Transpose(char[][] a) {
        var answer = new char[a[0].Length][];
        for (int i = 0; i < a[0].Length; ++i) answer[i] = new char[a.Length];
        for (int i = 0; i < a.Length; ++i)
            for (int j = 0; j < a[0].Length; ++j)
                answer[j][i] = a[i][j];
        return answer;
    }
    
    static int LegalMove(char[][] a) {
        int answer = 0;
        foreach (var ai in a)
            for (int j = 0; j < a.Length - 2; ++j)
                if (ai[j    ] == 'o' && 
                    ai[j + 1] == 'o' && 
                    ai[j + 2] == '.' ||
                    ai[j    ] == '.' && 
                    ai[j + 1] == 'o' && 
                    ai[j + 2] == 'o') ++answer;
            
        return answer;
    }
    
    public static void Main() {
        var a = new char[7][];
        for (int i = 0; i < 7; ++i) a[i] = Console.ReadLine().ToCharArray();
        Console.WriteLine(LegalMove(a) + LegalMove(Transpose(a)));
    }
}
