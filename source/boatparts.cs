using System;
using System.Collections.Generic;

class boatparts {
	static string ReadSolve() {
		var line = Console.ReadLine().Split();
		var p = int.Parse(line[0]);
		var n = int.Parse(line[1]);
		var seen = new HashSet<string>();
		for (int i = 0; i < n; ++i) {
			seen.Add(Console.ReadLine());
			if (seen.Count == p) return (i + 1).ToString();
		}
		return "paradox avoided";
	}

	public static void Main() {
		Console.WriteLine(ReadSolve());
	}
}
