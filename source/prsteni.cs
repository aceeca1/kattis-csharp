using System;

class prsteni {
	public static void Main() {
		var n = int.Parse(Console.ReadLine());
		var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
		for (int i = 1; i < n; ++i) {
			var rational = new Rational {
				Numerator = a[0],
				Denominator = a[i]
			};
			rational.Simplify();
			Console.WriteLine($"{rational.Numerator}/{rational.Denominator}");
		}
	}
}
