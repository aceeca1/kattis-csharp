using System;

class twostones {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine((n & 1) == 0 ? "Bob" : "Alice");
    }
}
