using System;
using System.Linq;

class anewalphabet {
    static string[] Translate = {
        "@", "8", "(", "|)", "3", "#", "6",
        "[-]", "|", "_|", "|<", "1", "[]\\/[]", "[]\\[]", 
        "0", "|D", "(,)", "|Z", "$", "']['",
        "|_|", "\\/", "\\/\\/", "}{", "`/", "2"
    };
    
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(string.Concat(s.Select(k => {
            var kLower = char.ToLower(k);
            if (char.IsLower(kLower)) return Translate[kLower - 'a'];
            return kLower.ToString();
        })));
    }
}
