using System;

class grassseed {
    public static void Main() {
        var c = double.Parse(Console.ReadLine());
        var n = int.Parse(Console.ReadLine());
        double answer = 0;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var w = double.Parse(line[0]);
            var l = double.Parse(line[1]);
            answer += w * l;
        }
        Console.WriteLine(answer * c);
    }
}
