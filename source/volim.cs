using System;

class volim {
    public static void Main() {
        var k = int.Parse(Console.ReadLine());
        var total = 210;
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var t = int.Parse(line[0]);
            var z = line[1][0];
            total -= t;
            if (total < 0) break;
            if (z == 'T') ++k;
        }
        Console.WriteLine(((k + 7) & 7) + 1);
    }
}
