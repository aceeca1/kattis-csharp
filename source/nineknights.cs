using System;
using System.Linq;

class nineknights {
    static bool Attack(char[][] board) {
        for (int i = 0; i < board.Length - 2; ++i)
            for (int j = 0; j < board[0].Length - 1; ++j)
                if (board[i][j] == 'k' && board[i + 2][j + 1] == 'k' ||
                    board[i][j + 1] == 'k' && board[i + 2][j] == 'k')
                    return true;
        return false;
    }
    
    static char[][] Transpose(char[][] board) {
        var answer = new char[board[0].Length][];
        for (int i = 0; i < answer.Length; ++i) 
            answer[i] = new char[board.Length];
        for (int i = 0; i < board.Length; ++i)
            for (int j = 0; j < board[0].Length; ++j)
                answer[j][i] = board[i][j];
        return answer;
    }
    
    static bool Valid(char[][] board) {
        if (board.Sum(k => k.Count(k1 => k1 == 'k')) != 9) return false;
        return !Attack(board) && !Attack(Transpose(board));
    }
    
    public static void Main() {
        var board = new char[5][];
        for (int i = 0; i < 5; ++i) board[i] = Console.ReadLine().ToCharArray();
        Console.WriteLine(Valid(board) ? "valid" : "invalid");
    }
}
