using System;

class encodedmessage {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            var k = (int)Math.Sqrt(s.Length);
            var a = new char[k, k];
            int k1 = 0;
            for (int i1 = 0; i1 < k; ++i1)
                for (int i2 = k - 1; i2 >= 0; --i2)
                    a[i2, i1] = s[k1++];
            var a1 = new char[k * k];
            int k2 = 0;
            for (int i1 = 0; i1 < k; ++i1)
                for (int i2 = 0; i2 < k; ++i2)
                    a1[k2++] = a[i1, i2];
            Console.WriteLine(a1);
        }
    }
}
