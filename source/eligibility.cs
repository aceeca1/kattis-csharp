using System;

class eligibility {
    static string EligibleStatus(int studies, int birth, int courses) {
        if (studies >= 2010 || birth >= 1991) return "eligible";
        if (courses > 40) return "ineligible";
        return "coach petitions";
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine().Split();
            var name = s[0];
            var studies = int.Parse(s[1].Split('/')[0]);
            var birth   = int.Parse(s[2].Split('/')[0]);
            var courses = int.Parse(s[3]);
            var answer = EligibleStatus(studies, birth, courses);
            Console.WriteLine("{0} {1}", name, answer);
        }
    }
}
