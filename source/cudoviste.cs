using System;
using System.Linq;

class cudoviste {
    static bool CanSquash(char[] board, out int needSquash) {
        needSquash = 0;
        if (Array.IndexOf(board, '#') != -1) return false;
        needSquash = board.Count(k => k == 'X');
        return true;
    }
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var r = int.Parse(line[0]);
        var c = int.Parse(line[1]);
        var a = new string[r];
        for (int i = 0; i < r; ++i) a[i] = Console.ReadLine();
        var answer = new int[5];
        for (int i = 0; i < r - 1; ++i)
            for (int j = 0; j < c - 1; ++j) {
                int needSquash;
                if (CanSquash(new char[] {
                    a[i    ][j], a[i    ][j + 1], 
                    a[i + 1][j], a[i + 1][j + 1]
                }, out needSquash)) ++answer[needSquash];
            }
        for (int i = 0; i < 5; ++i) Console.WriteLine(answer[i]);
    }
}
