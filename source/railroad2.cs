using System;

class railroad2 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var y = int.Parse(line[1]);
        Console.WriteLine((y & 1) == 0 ? "possible" : "impossible");
    }
}
