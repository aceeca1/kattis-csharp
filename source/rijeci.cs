using System;

class rijeci {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        long a = 1, b = 0;
        for (int i = 0; i < n; ++i) {
            var a0 = a;
            a = b;
            b = a0 + b;
        }
        Console.WriteLine("{0} {1}", a, b);
    }
}
