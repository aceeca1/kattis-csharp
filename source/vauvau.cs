using System;

class vauvau {
    static int WillAttack(int m, int a, int b) =>
        (m + a + b - 1) % (a + b) < a ? 1 : 0;

    static string[] Describe = {"none", "one", "both"};
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        var d = int.Parse(line[3]);
        var pmg = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        foreach (var i in pmg) {
            var attack = WillAttack(i, a, b) + WillAttack(i, c, d);
            Console.WriteLine(Describe[attack]);
        }
    }
}
