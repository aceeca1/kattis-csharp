using System;
using System.Linq;

class cetvrta {
    public static void Main() {
        var a1 = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var a2 = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var a3 = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var answer = Enumerable.Range(0, 2).Select(k => a1[k] ^ a2[k] ^ a3[k]);
        Console.WriteLine(string.Join(" ", answer));
    }
}
