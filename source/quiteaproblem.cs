using System;

class quiteaproblem {
    public static void Main() {
        while (true) {
            var s = Console.ReadLine();
            if (s == null) break;
            Console.WriteLine(s.ToLower().Contains("problem") ? "yes" : "no");
        }
    }
}
