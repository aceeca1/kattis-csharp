using System;
using System.Linq;

class tarifa {
    public static void Main() {
        var x = int.Parse(Console.ReadLine());
        var n = int.Parse(Console.ReadLine());
        var a = Enumerable.Range(0, n)
                          .Sum(k => int.Parse(Console.ReadLine()));
        Console.WriteLine(x * (n + 1) - a);
    }
}
