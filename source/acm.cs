using System;

class acm {
    public static void Main() {
        var a = new int[26];
        int answer1 = 0, answer2 = 0;
        while (true) {
            var line = Console.ReadLine();
            if (line == "-1") break;
            var lineS = line.Split();
            var time = int.Parse(lineS[0]);
            var problem = lineS[1][0] - 'A';
            var submission = lineS[2];
            if (a[problem] == -1) continue;
            switch (submission) {
                case "right":
                    ++answer1; 
                    answer2 += a[problem] + time;
                    a[problem] = -1; break;
                case "wrong": a[problem] += 20; break;
            }
        }
        Console.WriteLine("{0} {1}", answer1, answer2);
    }
}
