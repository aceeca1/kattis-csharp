using System;

class numberfun {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var a = int.Parse(line[0]);
            var b = int.Parse(line[1]);
            var c = int.Parse(line[2]);
            bool answer = a + b == c || a - b == c || b - a == c ||
                          a * b == c || a == b * c || b == a * c;
            Console.WriteLine(answer ? "Possible" : "Impossible");
        }
    }
}
