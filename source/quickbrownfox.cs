using System;
using System.Collections.Generic;

class quickbrownfox {
    static IEnumerable<char> Missing(int found) {
        for (int i = 0; i < 26; ++i)
            if (((found >> i) & 1) == 0) yield return (char)('a' + i);
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var found = 0;
            foreach (var j in Console.ReadLine()) {
                var c = char.ToLower(j);
                if (char.IsLower(c)) found |= 1 << (c - 'a');
            }
            var missing = string.Concat(Missing(found));
            var answer = missing.Length == 0 ? "pangram" : $"missing {missing}";
            Console.WriteLine(answer);
        }
    }
}
