using System;
using System.Collections.Generic;

class timebomb {
    static string[] LCD = {
        "***   * *** *** * * *** *** *** *** ***",
        "* *   *   *   * * * *   *     * * * * *",
        "* *   * *** *** *** *** ***   * *** ***",
        "* *   * *     *   *   * * *   * * *   *",
        "***   * *** ***   * *** ***   * *** ***"
    };
    
    static string[] ReadLCD(string[] lcd) {
        var answer = new string[(lcd[0].Length + 1) >> 2];
        for (int i = 0; i < answer.Length; ++i) {
            var answerI = new char[15];
            int k = 0;
            for (int j1 = 0; j1 < 3; ++j1)
                for (int j2 = 0; j2 < 5; ++j2)
                    answerI[k++] = lcd[j2][(i << 2) + j1];
            answer[i] = new string(answerI);
        }
        return answer;
    }

    static bool OCR(Dictionary<string, int> ocr, string[] b, out int answer) {
        answer = 0;
        foreach (var i in b) {
            int v;
            if (ocr.TryGetValue(i, out v)) answer = answer * 10 + v;
            else return false;
        }
        return true;
    }
    
    public static void Main() {
        var sample = ReadLCD(LCD);
        var ocr = new Dictionary<string, int>();
        for (int i = 0; i < sample.Length; ++i) ocr[sample[i]] = i;
        var a = new string[5];
        for (int i = 0; i < 5; ++i) a[i] = Console.ReadLine();
        var b = ReadLCD(a);
        int number;
        if (OCR(ocr, b, out number) && number % 6 == 0) 
            Console.WriteLine("BEER!!");
        else Console.WriteLine("BOOM!!");
    }
}
