using System;

class hissingmicrophone {
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(s.Contains("ss") ? "hiss" : "no hiss");
    }
}
