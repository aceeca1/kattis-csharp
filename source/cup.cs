using System;

class cup {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var name = new string[n];
        var size = new int[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            if (char.IsLetter(line[0][0])) {
                name[i] = line[0];
                size[i] = int.Parse(line[1]);
            } else {
                name[i] = line[1];
                size[i] = int.Parse(line[0]) >> 1;
            }
        }
        Array.Sort(size, name);
        foreach (var i in name) Console.WriteLine(i);
    }
}
