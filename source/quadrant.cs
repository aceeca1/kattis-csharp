using System;

class quadrant {
    public static void Main() {
        var x = int.Parse(Console.ReadLine());
        var y = int.Parse(Console.ReadLine());
        if (0 < x) Console.WriteLine(0 < y ? 1 : 4);
        else       Console.WriteLine(0 < y ? 2 : 3);
    }
}
