using System;

class tetris {
    static int[][][] TetrisBottom = {
        new[]{new[]{0}, new[]{0, 0, 0, 0}},
        new[]{new[]{0, 0}},
        new[]{new[]{0, 0, 1}, new[]{1, 0}},
        new[]{new[]{1, 0, 0}, new[]{0, 1}},
        new[]{new[]{0, 0, 0}, new[]{1, 0}, new[]{1, 0, 1}, new[]{0, 1}},
        new[]{new[]{0, 0, 0}, new[]{2, 0}, new[]{0, 1, 1}, new[]{0, 0}},
        new[]{new[]{0, 0, 0}, new[]{0, 2}, new[]{1, 1, 0}, new[]{0, 0}}
    };

    static bool Fit(int[] a, int k, int[] b) {
        var h = a[k] - b[0];
        for (int i = 1; i < b.Length; ++i)
            if (a[k + i] - b[i] != h) return false;
        return true;
    }
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var p = int.Parse(line[1]) - 1;
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int answer = 0;
        foreach (var pi in TetrisBottom[p])
            for (int i = 0; i < a.Length - pi.Length + 1; ++i)
                if (Fit(a, i, pi)) ++answer;
        Console.WriteLine(answer);
    }
}
