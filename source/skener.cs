using System;
using System.Linq;

class skener {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var r = int.Parse(line[0]);
        var zr = int.Parse(line[2]);
        var zc = int.Parse(line[3]);
        for (int i = 0; i < r; ++i) {
            var s = Console.ReadLine();
            s = string.Concat(s.SelectMany(k => Enumerable.Repeat(k, zc)));
            for (int j = 0; j < zr; ++j) Console.WriteLine(s);
        }
    }
}
