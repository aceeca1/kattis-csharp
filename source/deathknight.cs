using System;

class deathknight {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int answer = 0;
        for (int i = 0; i < n; ++i)
            if (!Console.ReadLine().Contains("CD")) ++answer;
        Console.WriteLine(answer);
    }
}
