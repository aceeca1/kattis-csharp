using System;

class conundrum {
    public static void Main() {
        var s = Console.ReadLine();
        int answer = 0;
        for (int i = 0; i < s.Length; ++i)
            if ("PER"[i % 3] != s[i]) ++answer;
        Console.WriteLine(answer);
    }
}
