using System;

class savingforretirement {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var b = int.Parse(line[0]);
        var bR = int.Parse(line[1]);
        var bS = int.Parse(line[2]);
        var a = int.Parse(line[3]);
        var aS = int.Parse(line[4]);
        var bMoney = (bR - b) * bS;
        var aMoney = bMoney + 1;
        var aR = a + (aMoney + aS - 1) / aS;
        Console.WriteLine(aR);
    }
}
