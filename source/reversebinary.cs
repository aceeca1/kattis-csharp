using System;

class reversebinary {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int answer = 0;
        for (; n != 0; n >>= 1) answer += answer + (n & 1);
        Console.WriteLine(answer);
    }
}
