using System;

class savingdaylight {
    static int ParseTime(string time) {
        var timeS = time.Split(':');
        var h = int.Parse(timeS[0]);
        var m = int.Parse(timeS[1]);
        return h * 60 + m;
    }

    public static void Main() {
        while (true) {
            var line = Console.ReadLine();
            if (line == null) break;
            var lineS = line.Split();
            var sunrise = ParseTime(lineS[3]);
            var sunset = ParseTime(lineS[4]);
            var daylight = sunset - sunrise;
            Console.WriteLine("{0} {1} {2} {3} hours {4} minutes",
                lineS[0], lineS[1], lineS[2],
                daylight / 60, daylight % 60);
        }
    }
}
