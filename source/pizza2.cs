using System;

class pizza2 {
    static double Square(double x) => x * x;
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var r = double.Parse(line[0]);
        var c = double.Parse(line[1]);
        Console.WriteLine(Square((r - c) / r) * 100.0);
    }
}
