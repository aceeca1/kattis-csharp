using System;
using System.Linq;

class filip {
    static string Reverse(string s) {
        var a = s.ToCharArray();
        Array.Reverse(a);
        return new string(a);
    }
    
    public static void Main() {
        var a = Console.ReadLine().Split();
        Console.WriteLine(a.Max(k => int.Parse(Reverse(k))));
    }
}
