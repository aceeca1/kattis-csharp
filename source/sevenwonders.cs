using System;
using System.Linq;

class sevenwonders {
    public static void Main() {
        var a = new int[26];
        foreach (var i in Console.ReadLine()) ++a[i - 'A'];
        var suit = "TCG".Min(k => a[k - 'A']);
        Console.WriteLine(a.Sum(k => k * k) + suit * 7);
    }
}
