using System;
using System.Linq;

class helpaphd {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine();
            if (line == "P=NP") Console.WriteLine("skipped");
            else Console.WriteLine(line.Split('+').Sum(int.Parse));
        }
    }
}
