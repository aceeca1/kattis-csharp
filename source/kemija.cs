using System;
using System.Collections.Generic;

class kemija {
    static IEnumerable<char> Decode(string s) {
        var position = 0;
        while (position < s.Length) {
            yield return s[position];
            if ("aeiou".IndexOf(s[position]) == -1) ++position;
            else position += 3;
        }
    }
    
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(string.Concat(Decode(s)));
    }
}
