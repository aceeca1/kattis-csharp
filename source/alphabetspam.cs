using System;

class alphabetspam {
    public static void Main() {
        int whitespace = 0, lower = 0, upper = 0, symbol = 0;
        var s = Console.ReadLine();
        foreach (var i in s)
            if (i == '_') ++whitespace;
            else if (char.IsLower(i)) ++lower;
            else if (char.IsUpper(i)) ++upper;
            else ++symbol;
        Console.WriteLine((double)whitespace / s.Length);
        Console.WriteLine((double)lower      / s.Length);
        Console.WriteLine((double)upper      / s.Length);
        Console.WriteLine((double)symbol     / s.Length);
    }
}
