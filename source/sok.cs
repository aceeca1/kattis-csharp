using System;
using System.Linq;

class sok {
	public static void Main() {
		var amount = Array.ConvertAll(Console.ReadLine().Split(), double.Parse);
		var ratio = Array.ConvertAll(Console.ReadLine().Split(), double.Parse);
		var unit = Enumerable.Range(0, 3)
		                     .Min(k => amount[k] / ratio[k]);
		var left = Enumerable.Range(0, 3)
		                     .Select(k => amount[k] - ratio[k] * unit);
		Console.WriteLine(string.Join(" ", left));
	}
}
