using System;
using System.Linq;

class tolower {
    static bool ReadTest(int t) {
        bool answer = true;
        for (int i = 0; i < t; ++i)
            if (!Console.ReadLine().Skip(1).All(k => char.IsLower(k)))
                answer = false;
        return answer;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var p = int.Parse(line[0]);
        var t = int.Parse(line[1]);
        var answer = 0;
        for (int i = 0; i < p; ++i)
            if (ReadTest(t)) ++answer;
        Console.WriteLine(answer);
    }
}
