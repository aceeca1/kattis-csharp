using System;

class estimatingtheareaofacircle {
    public static void Main() {
        while (true) {
            var line = Console.ReadLine().Split();
            var r = double.Parse(line[0]);
            var m = double.Parse(line[1]);
            var c = double.Parse(line[2]);
            if (r == 0.0) break;
            var answer1 = Math.PI * r * r;
            var answer2 = r * r * 4.0 * c / m;
            Console.WriteLine("{0} {1}", answer1, answer2);
        }
    }
}
