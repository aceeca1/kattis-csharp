using System;

class skocimis {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        Console.WriteLine(Math.Max(b - a, c - b) - 1);
    }
}
