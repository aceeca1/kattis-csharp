using System;

class differentdistances {
    public static void Main() {
        while (true) {
            var line = Console.ReadLine();
            if (line == "0") break;
            var lineS = line.Split();
            var x1 = double.Parse(lineS[0]);
            var y1 = double.Parse(lineS[1]); 
            var x2 = double.Parse(lineS[2]);
            var y2 = double.Parse(lineS[3]);
            var p  = double.Parse(lineS[4]);
            var aX = Math.Pow(Math.Abs(x1 - x2), p);
            var aY = Math.Pow(Math.Abs(y1 - y2), p);
            Console.WriteLine(Math.Pow(aX + aY, 1.0 / p));
        }
    }
}
