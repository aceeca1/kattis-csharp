using System;
using System.Collections.Generic;
using System.Linq;

class patuljci {
    public static void Main() {
        var a = new int[9];
        for (int i = 0; i < 9; ++i) a[i] = int.Parse(Console.ReadLine());
        var sum = a.Sum();
        var h = new HashSet<int>(a);
        int k1 = 0, a1, a2;
        while (true) {
            a1 = a[k1];
            a2 = sum - a1 - 100;
            if (a1 != a2 && h.Contains(a2)) break;
            ++k1;
        }
        foreach (var i in a) 
            if (i != a1 && i != a2) Console.WriteLine(i);
    }
}
