using System;
using System.Linq;

class modulo {
    public static void Main() {
        Console.WriteLine(Enumerable.Range(0, 10).Select(
            k => int.Parse(Console.ReadLine()) % 42).Distinct().Count());
    }
}
