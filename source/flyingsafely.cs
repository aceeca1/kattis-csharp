using System;

class flyingsafely {
	public static void Main() {
		var cases = int.Parse(Console.ReadLine());
		for (int caseNo = 1; caseNo <= cases; ++caseNo) {
			var line = Console.ReadLine().Split();
			var n = int.Parse(line[0]);
			var m = int.Parse(line[1]);
			for (int i = 0; i < m; ++i) Console.ReadLine();
			Console.WriteLine(n - 1);
		}
	}
}
