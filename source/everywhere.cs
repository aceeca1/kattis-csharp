using System;
using System.Linq;

class everywhere {
    public static void Main() {
        var t = int.Parse(Console.ReadLine());
        for (int i = 0; i < t; ++i) {
            var n = int.Parse(Console.ReadLine());
            var a = Enumerable.Range(0, n).Select(k => Console.ReadLine());
            Console.WriteLine(a.Distinct().Count());
        }
    }
}
