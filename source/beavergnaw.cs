using System;

class beavergnaw {
    public static void Main() {
        while (true) {
            var line = Console.ReadLine().Split();
            var d = double.Parse(line[0]);
            var v = double.Parse(line[1]);
            if (d == 0.0) break;
            var a = d * d * d - 6.0 / Math.PI * v;
            Console.WriteLine(Math.Pow(a, 1.0 / 3.0));
        }        
    }
}
