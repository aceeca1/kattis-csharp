using System;

class oddgnome {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
            int k = 1;
            while (a[k] + 1 == a[k + 1]) ++k;
            Console.WriteLine(k + 1);
        }
    }
}
