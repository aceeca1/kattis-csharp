using System;

class redrover {
    static int Compress(string macro, string s) =>
        s.Replace(macro, "M").Length + macro.Length;

    public static void Main() {
        var s = Console.ReadLine();
        var answer = s.Length;
        for (int i = 0; i < s.Length; ++i)
            for (int j = 2; i + j <= s.Length; ++j) {
                var compress = Compress(s.Substring(i, j), s);
                if (compress < answer) answer = compress;
            }
        Console.WriteLine(answer);
    }
}
