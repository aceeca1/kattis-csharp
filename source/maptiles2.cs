using System;

class maptiles2 {
    public static void Main() {
        int x = 0, y = 0;
        var line = Console.ReadLine();
        foreach (var i in line) switch (i) {
            case '0': x += x;      y += y;     break;
            case '1': x += x + 1;  y += y;     break;
            case '2': x += x;      y += y + 1; break;
            case '3': x += x + 1;  y += y + 1; break;
        }
        Console.WriteLine("{0} {1} {2}", line.Length, x, y);
    }
}
