using System;

class humancannonball2 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var v0 = double.Parse(line[0]);
            var a  = double.Parse(line[1]);
            var x  = double.Parse(line[2]);
            var h1 = double.Parse(line[3]);
            var h2 = double.Parse(line[4]);
            var aR = a * (Math.PI / 180.0);
            var t = x / (v0 * Math.Cos(aR));
            var y = v0 * t * Math.Sin(aR) - 0.5 * 9.81 * t * t;
            var safe = h1 + 1.0 <= y && y + 1.0 <= h2;
            Console.WriteLine(safe ? "Safe" : "Not Safe");
        }
    }
}
