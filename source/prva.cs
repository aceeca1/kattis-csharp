using System;

class prva {
    class Crossword {
        char[][] Board;

        public static Crossword Read() {
            var line = Console.ReadLine().Split();
            var r = int.Parse(line[0]);
            var answer = new Crossword { Board = new char[r][] };
            for (int i = 0; i < r; ++i) 
                answer.Board[i] = Console.ReadLine().ToCharArray();
            return answer;
        }
        
        public void Transpose() {
            var newBoard = new char[Board[0].Length][];
            for (int i = 0; i < newBoard.Length; ++i) 
                newBoard[i] = new char[Board.Length];
            for (int i = 0; i < Board.Length; ++i)
                for (int j = 0; j < Board[0].Length; ++j)
                    newBoard[j][i] = Board[i][j];
            Board = newBoard;
        }
        
        public string GetMin() {
            string minCrossword = null;
            for (int i = 0; i < Board.Length; ++i)
                for (int j = 0; j < Board[0].Length; ++j) {
                    if (j != 0 && Board[i][j - 1] != '#') continue;
                    int j1 = j;
                    while (j1 != Board[0].Length && Board[i][j1] != '#') ++j1;
                    if (2 <= j1 - j) {
                        var s = new string(Board[i], j, j1 - j);
                        if (minCrossword == null || 
                            string.Compare(s, minCrossword) < 0)
                            minCrossword = s;
                    }
                }
            return minCrossword;
        }
    }
    
    static string Minimum(string s1, string s2) {
        if (s1 == null) return s2;
        if (s2 == null) return s1;
        return string.Compare(s1, s2) < 0 ? s1 : s2;
    }
    
    public static void Main() {
        var crossword = Crossword.Read();
        var answer1 = crossword.GetMin();
        crossword.Transpose();
        var answer2 = crossword.GetMin();
        Console.WriteLine(Minimum(answer1, answer2));
    }
}
