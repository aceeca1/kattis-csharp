using System;
using System.Collections.Generic;

class chopin {
    static string[] A1 = {"A#", "C#", "D#", "F#", "G#"};
    static string[] A2 = {"Bb", "Db", "Eb", "Gb", "Ab"};
    static Dictionary<string, string> A = new Dictionary<string, string>();

    public static void Main() {
        for (int i = 0; i < A1.Length; ++i) {
            A[A1[i]] = A2[i];
            A[A2[i]] = A1[i];
        }
        for (int i = 1;; ++i) {
            var line = Console.ReadLine();
            if (line == null) break;
            var lineS = line.Split();
            var note = lineS[0];
            var tonality = lineS[1];
            string v;
            if (A.TryGetValue(note, out v)) 
                Console.WriteLine("Case {0}: {1} {2}", i, v, tonality);
            else Console.WriteLine("Case {0}: UNIQUE", i);
        }
    }
}
