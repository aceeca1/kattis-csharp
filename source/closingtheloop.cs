using System;
using System.Collections.Generic;
using System.Linq;

class closingtheloop {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 1; i <= n; ++i) {
            Console.ReadLine();
            var r = new List<int>();
            var b = new List<int>();
            foreach (var j in Console.ReadLine().Split()) {
                var length = int.Parse(j.Substring(0, j.Length - 1));
                switch (j[j.Length - 1]) {
                    case 'R': r.Add(length); break;
                    case 'B': b.Add(length); break;
                }
            }
            r.Sort();
            r.Reverse();
            b.Sort();
            b.Reverse();
            var m = Math.Min(r.Count, b.Count);
            var answer = (r.Take(m).Sum() - m) + (b.Take(m).Sum() - m);
            Console.WriteLine("Case #{0}: {1}", i, answer);
        }
    }
}
