using System;
using System.Linq;

class parking {
    public static void Main() {
        var price = new int[4];
        var line = Console.ReadLine().Split();
        for (int i = 1; i < 4; ++i) price[i] = i * int.Parse(line[i - 1]);
        var truck = new int[3][];
        for (int i = 0; i < 3; ++i)
            truck[i] = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int max = truck.Max(k => k[1]);
        var histogram = new int[max];
        for (int i = 0; i < 3; ++i)
            for (int j = truck[i][0]; j < truck[i][1]; ++j) ++histogram[j];
        Console.WriteLine(histogram.Sum(k => price[k]));
    }
}
