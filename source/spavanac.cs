using System;

class spavanac {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var h = int.Parse(line[0]);
        var m = int.Parse(line[1]) - 45;
        if (m < 0) { m += 60; h -= 1; }
        if (h < 0) h += 24;
        Console.WriteLine("{0} {1}", h, m);
    }
}
