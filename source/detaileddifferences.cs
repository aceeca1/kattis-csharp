using System;
using System.Linq;

class detaileddifferences {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var s1 = Console.ReadLine();
            var s2 = Console.ReadLine();
            Console.WriteLine(s1);
            Console.WriteLine(s2);
            var range = Enumerable.Range(0, s1.Length);
            var answer = range.Select(k => s1[k] == s2[k] ? '.' : '*');
            Console.WriteLine(string.Concat(answer));
            Console.WriteLine();
        }
    }
}
