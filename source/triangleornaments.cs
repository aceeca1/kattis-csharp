using System;

class triangleornaments {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var answer = 0.0;
        for (int i = 0; i < n; ++i) {
            var a = Array.ConvertAll(Console.ReadLine().Split(), double.Parse);
            var p = 0.5 * (a[0] + a[1] + a[2]);
            var s = Math.Sqrt(p * (p - a[0]) * (p - a[1]) * (p - a[2]));
            var k = (a[0] * a[0] + a[1] * a[1]) * 2.0 - a[2] * a[2];
            var m = 0.5 * Math.Sqrt(k);
            answer += (s + s) / m;
        }
        Console.WriteLine(answer);
    }
}
