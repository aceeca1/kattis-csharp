using System;
using System.Collections.Generic;

class veci {
    public static void Main() {
        var s = Console.ReadLine().ToCharArray();
        if (new Permute<char>{Comparer = Comparer<char>.Default}.Next(s))
            Console.WriteLine(s);
        else Console.WriteLine(0);
    }
}
