using System;

class zanzibar {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
            int answer = 0;
            for (int j = 0; j <= a.Length - 2; ++j)
                answer += Math.Max(0, a[j + 1] - (a[j] << 1));
            Console.WriteLine(answer);
        }
    }
}
