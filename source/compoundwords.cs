using System;
using System.Collections.Generic;

class compoundwords {
    public static void Main() {
        var a = new List<string>();
        while (true) {
            var line = Console.ReadLine();
            if (line == null) break;
            a.AddRange(line.Split());
        }
        var s = new SortedSet<string>();
        for (int i1 = 0; i1 < a.Count; ++i1)
            for (int i2 = i1 + 1; i2 < a.Count; ++i2) {
                s.Add(a[i1] + a[i2]);
                s.Add(a[i2] + a[i1]);
            }
        foreach (var i in s) Console.WriteLine(i);
    }
}
