using System;

class bus {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var a = int.Parse(Console.ReadLine());
            Console.WriteLine((1 << a) - 1);
        }
    }
}
