using System;

class speedlimit {
    public static void Main() {
        while (true) {
            var n = int.Parse(Console.ReadLine());
            if (n == -1) break;
            int time0 = 0, answer = 0;
            for (int i = 0; i < n; ++i) {
                var line = Console.ReadLine().Split();
                var s = int.Parse(line[0]);
                var t = int.Parse(line[1]);
                answer += s * (t - time0);
                time0 = t;
            }
            Console.WriteLine("{0} miles", answer);
        }
    }
}
