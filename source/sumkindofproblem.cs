using System;

class sumkindofproblem {
    public static void Main() {
        var t = int.Parse(Console.ReadLine());
        for (int i = 0; i < t; ++i) {
            var line = Console.ReadLine().Split();
            var k = int.Parse(line[0]);
            var n = int.Parse(line[1]);
            var s1 = n * (n + 1) >> 1;
            var s2 = n * n;
            var s3 = n * (n + 1);
            Console.WriteLine("{0} {1} {2} {3}", k, s1, s2, s3);
        }
    }
}
