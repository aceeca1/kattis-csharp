using System;

class keytocrypto {
    public static void Main() {
        var ciphertext = Console.ReadLine();
        var key = Console.ReadLine();
        var a = new char[key.Length + ciphertext.Length];
        for (int i = 0; i < key.Length; ++i) a[i] = key[i];
        for (int i = 0; i < ciphertext.Length; ++i)
            a[key.Length + i] = (char)((ciphertext[i] + 26 - a[i]) % 26 + 'A');
        Console.WriteLine(new string(a, key.Length, ciphertext.Length));
    }
}
