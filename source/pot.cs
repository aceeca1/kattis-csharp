using System;
using System.Linq;

class pot {
    public static void Main() {
        var answer = 0;
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine();
            var a = int.Parse(line.Substring(0, line.Length - 1));
            var b = line[line.Length - 1] - '0';
            answer += Enumerable.Repeat(a, b).Aggregate(1, (k1, k2) => k1 * k2);
        }
        Console.WriteLine(answer);
    }
}
