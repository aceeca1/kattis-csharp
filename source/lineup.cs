using System;

class lineup {
    static bool Increasing(string[] a) {
        for (int i = 1; i < a.Length; ++i)
            if (a[i].CompareTo(a[i - 1]) <= 0) return false;
        return true;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new string[n];
        for (int i = 0; i < n; ++i) a[i] = Console.ReadLine();
        if (Increasing(a)) Console.WriteLine("INCREASING");
        else {
            Array.Reverse(a);
            if (Increasing(a)) Console.WriteLine("DECREASING");
            else Console.WriteLine("NEITHER");
        }
    }
}
