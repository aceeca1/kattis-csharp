using System;

class nastyhacks {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var r = int.Parse(line[0]);
            var e = int.Parse(line[1]);
            var c = int.Parse(line[2]);
            e -= c;
            if (r < e)       Console.WriteLine("advertise");
            else if (r == e) Console.WriteLine("does not matter");
            else             Console.WriteLine("do not advertise");
        }
    }
}
