using System;

class tri {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        if (a + b == c) Console.WriteLine("{0}+{1}={2}", a, b, c);
        else if (a * b == c) Console.WriteLine("{0}*{1}={2}", a, b, c);
        else if (a == b + c) Console.WriteLine("{0}={1}+{2}", a, b, c);
        else if (a == b - c) Console.WriteLine("{0}={1}-{2}", a, b, c);
        else if (a == b * c) Console.WriteLine("{0}={1}*{2}", a, b, c);
        else if (a * c == b) Console.WriteLine("{0}={1}/{2}", a, b, c);
    }
}
