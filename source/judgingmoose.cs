using System;

class judgingmoose {
    public static void Main() {
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        if (a[0] == 0 && a[1] == 0) Console.WriteLine("Not a moose");
        else if (a[0] == a[1]) Console.WriteLine("Even {0}", a[0] << 1);
        else Console.WriteLine("Odd {0}", Math.Max(a[0], a[1]) << 1);
    }
}
