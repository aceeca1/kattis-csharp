using System;

class reverserot {
    const string Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_.";
    static int[] LetterIndex = new int[256];
    
    public static void Main() {
        for (int i = 0; i < Letters.Length; ++i) LetterIndex[Letters[i]] = i;
        while (true) {
            var line = Console.ReadLine();
            if (line == "0") break;
            var lineS = line.Split();
            var n = int.Parse(lineS[0]);
            var a = lineS[1].ToCharArray();
            Array.Reverse(a);
            for (int i = 0; i < a.Length; ++i)
                a[i] = Letters[(LetterIndex[a[i]] + n) % Letters.Length];
            Console.WriteLine(a);
        }
    }
}
