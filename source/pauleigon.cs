using System;

class pauleigon {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var p = int.Parse(line[1]);
        var q = int.Parse(line[2]);
        Console.WriteLine((p + q) % (n + n) < n ? "paul" : "opponent");
    }
}
