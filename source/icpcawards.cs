using System;
using System.Collections.Generic;

class icpcawards {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new Dictionary<string, string>();
        int count = 0;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine();
            var lineS = line.Split();
            string s;
            if (!a.TryGetValue(lineS[0], out s)) {
                a[lineS[0]] = lineS[1];
                Console.WriteLine(line);
                ++count;
            }
            if (count == 12) break;
        }
    }
}
