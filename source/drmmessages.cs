using System;
using System.Linq;

class drmmessages {
    static string Rotate(string s) {
        var rotation = s.Sum(k => k - 'A');
        var ss = s.Select(k => (char)((k - 'A' + rotation) % 26 + 'A'));
        return string.Concat(ss);
    }

    static string Merge(string s1, string s2) {
        var range = Enumerable.Range(0, s1.Length);
        var s = range.Select(k => 
            (char)(((s1[k] - 'A') + (s2[k] - 'A')) % 26 + 'A'));
        return string.Concat(s);
    }
    
    public static void Main() {
        var s = Console.ReadLine();
        var m = s.Length >> 1;
        var s1 = s.Substring(0, m);
        var s2 = s.Substring(m, m);
        Console.WriteLine(Merge(Rotate(s1), Rotate(s2)));
    }
}
