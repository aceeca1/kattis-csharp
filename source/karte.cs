using System;
using System.Linq;

class karte {
    static string Solve(string s) {
        var a = new int[4];
        for (int i = 0; i < s.Length; i += 3) {
            var suit = "PKHT".IndexOf(s[i]);
            var number = 1 << int.Parse(s.Substring(i + 1, 2));
            if ((a[suit] & number) != 0) return "GRESKA";
            a[suit] |= number;
        }
        return string.Join(" ", a.Select(k => 13 - Bit.Count14((uint)k)));
    }
    
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(Solve(s));
    }
}
