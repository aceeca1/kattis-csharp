using System;
using System.Linq;

class oddmanout {
    static int Xor(int a1, int a2) => a1 ^ a2;
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 1; i <= n; ++i) {
            Console.ReadLine();
            var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
            Console.WriteLine("Case #{0}: {1}", i, a.Aggregate(Xor));
        }
    }
}

solution 2:
using System;
using System.Collections.Generic;

class oddmanout {
	public static void Main() {
		var n = int.Parse(Console.ReadLine());
		for (int i = 0; i < n; ++i) {
			var notCouple = new HashSet<string>();
			var num = int.Parse(Console.ReadLine());
			var guest = Console.ReadLine().Split();
			for (int j = 0; j < num; ++j) {
				if (notCouple.Contains(guest[j])) {
					notCouple.Remove(guest[j]);
				}
				else notCouple.Add(guest[j]);
			}
			foreach (string j in notCouple) {
				Console.WriteLine("Case #{1}: {0}", j, i + 1);
			}
		}
	}
}
