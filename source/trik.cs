using System;

class trik {
    static int[,] Permute = {{1, 0, 2}, {0, 2, 1}, {2, 1, 0}};

    public static void Main() {
        int ball = 0;
        foreach (var i in Console.ReadLine()) ball = Permute[i - 'A', ball];
        Console.WriteLine(ball + 1);
    }
}
