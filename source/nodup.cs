using System;
using System.Collections.Generic;

class nodup {
    public static void Main() {
        var s = Console.ReadLine().Split();
        var z = new HashSet<string>(s);
        Console.WriteLine(z.Count == s.Length ? "yes" : "no");
    }
}
