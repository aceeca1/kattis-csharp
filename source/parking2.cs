using System;
using System.Linq;

class parking2 {
    public static void Main() {
        var t = int.Parse(Console.ReadLine());
        for (int i = 0; i < t; ++i) {
            Console.ReadLine();
            var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
            Console.WriteLine((a.Max() - a.Min()) << 1);
        }
    }
}
