using System;

class onechicken {
    const string LeftOver = "Dr. Chaz will have {0} {1} of chicken left over!";
    const string NeedMore = "Dr. Chaz needs {0} more {1} of chicken!";
    static string Piece(int number) => number == 1 ? "piece" : "pieces";
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        if (n < m) {
            var a = m - n;
            Console.WriteLine(LeftOver, a, Piece(a));
        } else {
            var a = n - m;
            Console.WriteLine(NeedMore, a, Piece(a));
        }
    }
}
