using System;
using System.Linq;

class peragrams {
    public static void Main() {
        var a = new int[256];
        foreach (var i in Console.ReadLine()) ++a[i];
        var odd = a.Count(k => (k & 1) != 0);
        Console.WriteLine(odd == 0 ? 0 : odd - 1);
    }
}
