using System;

class stararrangements {
    static void TryArrange(int n, int a, int b) {
        n %= a + b;
        if (n == 0 || n == a) Console.WriteLine("{0},{1}", a, b);
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine("{0}:", n);
        for (int i = 2; i < n; ++i) {
            TryArrange(n, i, i - 1);
            TryArrange(n, i, i);
        }
    }
}
