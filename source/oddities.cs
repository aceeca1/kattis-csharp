using System;

class oddities {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var a = int.Parse(Console.ReadLine());
            Console.WriteLine("{0} is {1}", a, (a & 1) == 0 ? "even" : "odd");
        }
    }
}
