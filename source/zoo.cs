using System;
using System.Collections.Generic;

class zoo {
    public static void Main() {
        for (int caseNo = 1;; ++caseNo) {
            var n = int.Parse(Console.ReadLine());
            if (n == 0) break;
            var a = new SortedDictionary<string, int>();
            for (int i = 0; i < n; ++i) {
                var line = Console.ReadLine().Split();
                var breed = line[line.Length - 1].ToLower();
                int v;
                a.TryGetValue(breed, out v);
                a[breed] = v + 1;
            }
            Console.WriteLine("List {0}:", caseNo);
            foreach (var i in a)
                Console.WriteLine("{0} | {1}", i.Key, i.Value);
        }
    }
}
