using System;
using System.Collections.Generic;

class musicalscales {
    static string[] Notes = {
        "A",  "A#", "B", "C",  "C#", "D",
        "D#", "E",  "F", "F#", "G",  "G#"
    };
    static Dictionary<string, int> NotesNo = new Dictionary<string, int>();
    static bool[] Tone = {
        true,  false, true,  false, true,  true,
        false, true,  false, true,  false, true
    };

    static bool Allow(bool[] song, int shift) {
        for (int i = 0; i < 12; ++i)
            if (song[i] && !Tone[(i + 12 - shift) % 12]) return false;
        return true;
    }

    static IEnumerable<string> PossibleScale(bool[] song) {
        for (int i = 0; i < 12; ++i)
            if (Allow(song, i)) yield return Notes[i];
    }

    public static void Main() {
        for (int i = 0; i < 12; ++i) NotesNo[Notes[i]] = i;
        Console.ReadLine();
        var song = new bool[12];
        foreach (var i in Console.ReadLine().Trim().Split())
            song[NotesNo[i]] = true;
        var answer = string.Join(" ", PossibleScale(song));
        Console.WriteLine(answer.Length == 0 ? "none" : answer);
    }
}
