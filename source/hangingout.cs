using System;

class hangingout {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var l = int.Parse(line[0]);
        var x = int.Parse(line[1]);
        int answer = 0;
        for (int i = 0; i < x; ++i) {
            line = Console.ReadLine().Split();
            int number = int.Parse(line[1]);
            switch (line[0]) {
                case "enter":
                    if (l - number < 0) ++answer;
                    else l -= number; break;
                case "leave": l += number; break;
            }
        }
        Console.WriteLine(answer);
    }
}
