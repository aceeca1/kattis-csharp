using System;
using System.Collections.Generic;
using System.Linq;

class ptice {
    class People {
        public string Name, Pattern;
        
        IEnumerable<char> Sequence() {
            while (true)
                foreach (var i in Pattern) yield return i;
        }
        
        public int Correct(string standard) {
            var sequence = Sequence().GetEnumerator();
            return standard.Count(k => {
                sequence.MoveNext();
                return k == sequence.Current;
            });
        }
    }
    
    static People[] people = {
        new People { Name = "Adrian", Pattern = "ABC"    },
        new People { Name = "Bruno" , Pattern = "BABC"   },
        new People { Name = "Goran" , Pattern = "CCAABB" }
    };
    
    public static void Main() {
        Console.ReadLine();
        var s = Console.ReadLine();
        var score = Array.ConvertAll(people, k => k.Correct(s));
        var maxScore = score.Max();
        Console.WriteLine(maxScore);
        for (int i = 0; i < people.Length; ++i)
            if (score[i] == maxScore) Console.WriteLine(people[i].Name);
    }
}
