using System;

class mirror {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 1; i <= n; ++i) {
            var line = Console.ReadLine().Split();
            var r = int.Parse(line[0]);
            var a = new char[r][];
            for (int j = 0; j < r; ++j) {
                a[j] = Console.ReadLine().ToCharArray();
                Array.Reverse(a[j]);
            }
            Array.Reverse(a);
            Console.WriteLine("Test {0}", i);
            foreach (var j in a) Console.WriteLine(j);
        }
    }
}
