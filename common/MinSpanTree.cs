using System.Collections.Generic;

interface IMSTQueue<Numeric> {
    void SetOps(IOps<Numeric> ops);
    void SetSize(int n);
    void SetRoot(int no);
    void Enqueue(int source, int target, Numeric cost);
    bool Dequeue(out int source, out int target, out Numeric cost);
}

class MSTQueueDense<Numeric>: IMSTQueue<Numeric> {
    int[] Parent;
    Numeric[] Cost;
    IOps<Numeric> Ops;

    public void SetOps(IOps<Numeric> ops) => Ops = ops;
    
    public void SetSize(int n) {
        Parent = new int[n];
        Cost = new Numeric[n];
        for (int i = 0; i < n; ++i) Cost[i] = Ops.MaxValue();
    }
    
    public void SetRoot(int no) => Parent[no] = -1;
    
    public void Enqueue(int source, int target, Numeric cost) {
        if (Parent[target] == -1) return;
        if (Ops.Less(cost, Cost[target])) {
            Cost[target] = cost;
            Parent[target] = source;
        }
    }
    
    public bool Dequeue(out int source, out int target, out Numeric cost) {
        source = -1;
        target = -1;
        cost = Ops.MaxValue();
        for (int i = 0; i < Parent.Length; ++i) 
            if (Parent[i] != -1 && Ops.Less(Cost[i], cost)) {
                cost = Cost[i];
                source = Parent[i];
                target = i;
            }
        if (source == -1) return false;
        Parent[target] = -1;
        return true;
    }
}

class MinSpanTree<Numeric> {
    public IGraph<ICostEdge<Numeric>> Graph;
    public IOps<Numeric> Ops;
    public EdgeListGraph<ICostEdge<Numeric>> Tree =
        new EdgeListGraph<ICostEdge<Numeric>>();
    public int TreeRoot = 0;
    public Numeric Total;

    public void Solve(IMSTQueue<Numeric> queue) {
        Total = Ops.Zero();
        for (int i = 0; i < Graph.SizeV(); ++i) 
            Tree.Out.Add(new List<ICostEdge<Numeric>>());
        queue.SetOps(Ops);
        queue.SetSize(Graph.SizeV());
        queue.SetRoot(TreeRoot);
        foreach (var i in Graph.OutEdge(TreeRoot)) 
            queue.Enqueue(TreeRoot, i.Target(), i.Cost());
        while (true) {
            int source, target;
            Numeric cost;
            if (!queue.Dequeue(out source, out target, out cost)) break;
            var newEdge = new CostEdge<Numeric> { T = target, C = cost };
            Tree.Out[source].Add(newEdge);
            Total = Ops.Add(Total, cost);
            foreach (var i in Graph.OutEdge(target))
                queue.Enqueue(target, i.Target(), i.Cost());
        }
    }
}
