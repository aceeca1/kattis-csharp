using System;
using System.Numerics;

interface IOps<T> {
    T From(int a);
    T Zero();
    T One();
    T MinusOne();
    T MinValue();
    T MaxValue();
    
    bool IsEven(T a);
    bool IsOne(T a);
    bool IsZero(T a);
    int Sign(T a);

    T Inc(T a);
    T Dec(T a);
    T Abs(T a);
    T Add(T a1, T a2);
    T Sub(T a1, T a2);
    T Mul(T a1, T a2);
    T Div(T a1, T a2);
    T Mod(T a1, T a2);
    T Shl(T a1, int a2);
    T Shr(T a1, int a2);
    T Max(T a1, T a2);
    T Min(T a1, T a2);

    bool Less(T a1, T a2);
    bool Eq(T a1, T a2);
}

class OpsInt: IOps<int> {
    public int From(int a) => a;
    public int Zero() => 0;
    public int One() => 1;
    public int MinusOne() => -1;
    public int MinValue() => int.MinValue;
    public int MaxValue() => int.MaxValue;
    
    public bool IsEven(int a) => (a & 1) == 0;
    public bool IsOne(int a) => a == 1;
    public bool IsZero(int a) => a == 0;
    public int Sign(int a) => a;

    public int Inc(int a) => a + 1;
    public int Dec(int a) => a - 1;
    public int Abs(int a) => System.Math.Abs(a);
    public int Add(int a1, int a2) => a1 + a2;
    public int Sub(int a1, int a2) => a1 - a2;
    public int Mul(int a1, int a2) => a1 * a2;
    public int Div(int a1, int a2) => a1 / a2;
    public int Mod(int a1, int a2) => a1 % a2;
    public int Shl(int a1, int a2) => a1 << a2;
    public int Shr(int a1, int a2) => a2 >> a2;
    public int Max(int a1, int a2) => System.Math.Max(a1, a2);
    public int Min(int a1, int a2) => System.Math.Min(a1, a2);

    public bool Less(int a1, int a2) => a1 < a2;
    public bool Eq(int a1, int a2) => a1 == a2;
}

class OpsLong : IOps<long> {
    public long From(int a) => a;
    public long Zero() => 0L;
    public long One() => 1L;
    public long MinusOne() => -1L;
    public long MinValue() => long.MinValue;
    public long MaxValue() => long.MaxValue;

    public bool IsEven(long a) => (a & 1L) == 0L;
    public bool IsOne(long a) => a == 1L;
    public bool IsZero(long a) => a == 0L;
    public int Sign(long a) => System.Math.Sign(a);

    public long Inc(long a) => a + 1L;
    public long Dec(long a) => a - 1L;
    public long Abs(long a) => System.Math.Abs(a);
    public long Add(long a1, long a2) => a1 + a2;
    public long Sub(long a1, long a2) => a1 - a2;
    public long Mul(long a1, long a2) => a1 * a2;
    public long Div(long a1, long a2) => a1 / a2;
    public long Mod(long a1, long a2) => a1 % a2;
    public long Shl(long a1, int a2) => a1 << a2;
    public long Shr(long a1, int a2) => a2 >> a2;
    public long Max(long a1, long a2) => System.Math.Max(a1, a2);
    public long Min(long a1, long a2) => System.Math.Min(a1, a2);

    public bool Less(long a1, long a2) => a1 < a2;
    public bool Eq(long a1, long a2) => a1 == a2;
}

class OpsDouble : IOps<double> {
    public double From(int a) => a;
    public double Zero() => 0.0;
    public double One() => 1.0;
    public double MinusOne() => -1.0;
    public double MinValue() => double.MinValue;
    public double MaxValue() => double.MaxValue;

    public bool IsEven(double a) { throw new NotImplementedException(); }
    public bool IsOne(double a) => a == 1.0;
    public bool IsZero(double a) => a == 0.0;
    public int Sign(double a) => System.Math.Sign(a);

    public double Inc(double a) => a + 1.0;
    public double Dec(double a) => a - 1.0;
    public double Abs(double a) => System.Math.Abs(a);
    public double Add(double a1, double a2) => a1 + a2;
    public double Sub(double a1, double a2) => a1 - a2;
    public double Mul(double a1, double a2) => a1 * a2;
    public double Div(double a1, double a2) => a1 / a2;
    public double Mod(double a1, double a2) => a1 % a2;
    public double Shl(double a1, int a2) => a1 * (1 << a2);
    public double Shr(double a1, int a2) => a1 / (1 << a2);
    public double Max(double a1, double a2) => System.Math.Max(a1, a2);
    public double Min(double a1, double a2) => System.Math.Min(a1, a2);

    public bool Less(double a1, double a2) => a1 < a2;
    public bool Eq(double a1, double a2) => a1 == a2;

}

class OpsBigInteger : IOps<BigInteger> {
    public BigInteger MinV = long.MinValue;
    public BigInteger MaxV = long.MaxValue;
    
    public BigInteger From(int a) => a;
    public BigInteger Zero() => BigInteger.Zero;
    public BigInteger One() => BigInteger.One;
    public BigInteger MinusOne() => BigInteger.MinusOne;
    public BigInteger MinValue() => MinV;
    public BigInteger MaxValue() => MaxV;

    public bool IsEven(BigInteger a) => a.IsEven;
    public bool IsOne(BigInteger a) => a.IsOne;
    public bool IsZero(BigInteger a) => a.IsZero;
    public int Sign(BigInteger a) => a.Sign;

    public BigInteger Inc(BigInteger a) => a + BigInteger.One;
    public BigInteger Dec(BigInteger a) => a - BigInteger.One;
    public BigInteger Abs(BigInteger a) => BigInteger.Abs(a);
    public BigInteger Add(BigInteger a1, BigInteger a2) => a1 + a2;
    public BigInteger Sub(BigInteger a1, BigInteger a2) => a1 - a2;
    public BigInteger Mul(BigInteger a1, BigInteger a2) => a1 * a2;
    public BigInteger Div(BigInteger a1, BigInteger a2) => a1 / a2;
    public BigInteger Mod(BigInteger a1, BigInteger a2) => a1 % a2;
    public BigInteger Shl(BigInteger a1, int a2) => a1 << a2;
    public BigInteger Shr(BigInteger a1, int a2) => a1 >> a2;
    public BigInteger Max(BigInteger a1, BigInteger a2) => BigInteger.Max(a1, a2);
    public BigInteger Min(BigInteger a1, BigInteger a2) => BigInteger.Min(a1, a2);

    public bool Less(BigInteger a1, BigInteger a2) => a1 < a2;
    public bool Eq(BigInteger a1, BigInteger a2) => a1 == a2;
}
