using System;
using System.Collections.Generic;

class Permute<T> {
    public IComparer<T> Comparer;

    public bool Next(T[] u) {
        if (u.Length <= 1) return false;
        int k = u.Length - 2;
        while (k != -1 && 0 <= Comparer.Compare(u[k], u[k + 1])) --k;
        if (k == -1) {
            Array.Reverse(u);
            return false;
        }
        int k1 = u.Length - 1;
        while (0 <= Comparer.Compare(u[k], u[k1])) --k1;
        T uk = u[k];
        u[k] = u[k1];
        u[k1] = uk;
        Array.Reverse(u, k + 1, u.Length - k - 1);
        return true;
    }
}
