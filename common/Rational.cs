using System.Numerics;

class Rational {
	public BigInteger Numerator, Denominator;
	
	public void Simplify() {
		var gcd = BigInteger.GreatestCommonDivisor(Numerator, Denominator);
		Numerator /= gcd;
		Denominator /= gcd;
	}
}
