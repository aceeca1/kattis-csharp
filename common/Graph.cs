using System.Collections.Generic;

interface IGraph<Edge> {
    int SizeV();
    IEnumerable<Edge> OutEdge(int no);
}

class EdgeListGraph<Edge>: IGraph<Edge> {
    public List<List<Edge>> Out = new List<List<Edge>>();
    public int SizeV() => Out.Count;
    public IEnumerable<Edge> OutEdge(int no) => Out[no];    
}

interface ICostEdge<Numeric> {
    int Target();
    Numeric Cost();
}

class CostEdge<Numeric>: ICostEdge<Numeric> {
    public int T;
    public Numeric C;
    public int Target() => T;
    public Numeric Cost() => C;
}
