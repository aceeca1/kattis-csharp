using System;
using System.Collections.Generic;

abstract class Partition<T, U> {
	protected abstract bool Equal(T t1, T t2);
	protected abstract U Single(T t);
	protected abstract U Add(U u, T t);
	
	public IEnumerable<U> Reduced(IEnumerable<T> input) {
        var data = default(T);
        var result = default(U);
        bool initial = true;
        foreach (var i in input)
            if (initial) {
                initial = false;
                data = i;
                result = Single(i);
            } else if (Equal(data, i)) {
                data = i;
                result = Add(result, i);
            } else {
                yield return result;
                data = i;
                result = Single(i);
            }
        if (!initial) yield return result;
	}
}
