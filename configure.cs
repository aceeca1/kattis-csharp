using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class Configure {
    static Dictionary<string, SortedSet<string>> Depend =
        new Dictionary<string, SortedSet<string>>();

    static string Csc = "csc", Rule1 = "rule csc", Rule2;
    static string Build = "build build/{0}.exe: csc source/{0}.cs";

    public static void Main() {
        if ((int)Environment.OSVersion.Platform == 4) Csc = "mcs";
        Rule2 = $"    command = {Csc} /out:$out /r:System.Numerics.dll $in";
        using (var file = new StreamReader("common/UsageList.txt")) {
            while (true) {
                var lineR = file.ReadLine();
                if (lineR == null) break;
                var line = lineR.Split();
                var lib = line[0].Substring(0, line[0].Length - 1);
                foreach (var source in line.Skip(1)) {
                    if (!Depend.ContainsKey(source))
                        Depend[source] = new SortedSet<string>();
                    Depend[source].Add(lib);
                }
            }
        }
        using (var file = new StreamWriter("build.ninja")) {
            file.WriteLine(Rule1);
            file.WriteLine(Rule2);
            file.WriteLine();
            foreach (var i in Directory.EnumerateFiles("source")) {
                var baseName = i.Split('\\', '/', '.')[1];
                file.Write(Build, baseName);
                SortedSet<string> libs;
                if (Depend.TryGetValue(baseName, out libs)) {
                    file.Write(' ');
                    var libsFull = libs.Select(k => $"common/{k}.cs");
                    file.WriteLine(string.Join(" ", libsFull));
                } else file.WriteLine();
            }
        }
    }
}
